<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Php Activity 1</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
    <h1>Full Address</h1>
        <p><?php echo getFullAddress('3F Caswynn Bldg.', 'Timog Avenue', 'Quezon City', 'Philippines') ?></p>
        <p><?php echo getFullAddress('3F Enzo Bldg.', 'Makati City', 'Metro Manila', 'Philippines') ?></p>

    <h1>Letter-Based Grading</h1>
        <p><?php echo letterGradingSystem(87); ?></p>
        <p><?php echo letterGradingSystem(94); ?></p>
        <p><?php echo letterGradingSystem(74); ?></p>
    
    </body>
</html>
